class ListManager {
    constructor() {
        this.ListDAO = require("../DAL/ListDAO");
        this.listDAO = new this.ListDAO();
    }

    getListById(id) {
        return new Promise((resolve, reject) => {
            try {
                var res = this.listDAO.getListById(id).then(list => {
                    return list;
                })
                resolve(res);
            } catch (error) {
                reject(error);

            }

        })

    }

    geAllLists() {
        return new Promise((resolve, reject) => {
            try {
                var res = this.listDAO.getAllLists().then(lists => {
                    return lists;
                })
                resolve(res);
            } catch (error) {
                reject(error);
            }

        })
    }
}

module.exports = ListManager

