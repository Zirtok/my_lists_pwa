class Item {
    constructor() {
        this.name=undefined;
        this.checked=false;
        this.id=undefined;
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }

    getChecked() {
        return this.checked;
    }

    setChecked(checked) {
        this.checked = checked;
    }


    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
    }


  }

  module.exports = Item

  