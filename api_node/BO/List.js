class List {
    constructor() {
        this.name = undefined;
        this.items = [];
        this.id = undefined;
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }

    getItems() {
        return this.items;
    }

    setItems(items) {
        this.items = items;
    }


    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
    }


}

module.exports = List

