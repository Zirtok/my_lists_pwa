class ListDAO {

    constructor() {
        this.List = require("../BO/List");
        this.Item = require("../BO/Item");
        this.sql = require("mssql")
        this.config = require("./config");
        this.reqGetListById = 'Select l.id as listId, l.[name] as listName, i.name as itemName, i.id as itemId, i.checked from LISTS l join ITEMS i on l.id = i.idList where l.id=';
        this.reqGetAllLists= 'Select l.id as listId, l.[name] as listName, i.name as itemName, i.id as itemId, i.checked from LISTS l join ITEMS i on l.id = i.idList;';
    }
    getListById(list_id) {
        return new Promise((resolve, reject) => {
            new this.sql.ConnectionPool(this.config).connect().then(pool => {
                return pool.query(this.reqGetListById + list_id)
            }).then(result => {
                try {
                    const list = new this.List();
                    list.setName(result.recordset[0].listName);
                    list.setId(result.recordset[0].listId);
    
                    result.recordset.forEach(row => {
                        const item = new this.Item();
                        item.setName(row.itemName);
                        item.setChecked(row.checked);
                        item.setId(row.itemId);
                        list.getItems().push(item);
                    });
                    resolve(list);
                } catch (error) {
                    reject(error);
                }
            },
                error => {
                    reject(error);
                }
            )

        })
    }

    getAllLists() {
        return new Promise((resolve, reject) => {
            new this.sql.ConnectionPool(this.config).connect().then(pool => {
                return pool.query(this.reqGetAllLists)
            }).then(result => {
                try {
                    const lists = [];
                    var idList;
                    var currentList;
                    result.recordset.forEach(row => {
                        if(row.listId !== idList) {
                            idList = row.listId;
                            const list = new this.List();
                            currentList = list;
                            currentList.setName(row.listName);
                            currentList.setId(row.listId);
                            
                            lists.push(currentList);
                        }
                        const item = new this.Item();
                        item.setName(row.itemName);
                        item.setChecked(row.checked);
                        item.setId(row.itemId);
                        currentList.getItems().push(item);
                    });
                    resolve(lists);
                } catch (error) {
                    reject(error);
                }
            },
                error => {
                    reject(error);
                }
            )

        })
    }

}

module.exports = ListDAO

