module.exports = {
    user: 'sa',
    password: 'Pa$$w0rd',
    server: 'localhost',
    database: 'Lists',
    pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 30000

    }

}