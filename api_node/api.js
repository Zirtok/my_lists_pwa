//L'application requiert l'utilisation du module Express.
//La variable express nous permettra d'utiliser les fonctionnalités du module Express.  
var express = require('express');
var ListManager = require("./BLL/ListManager");



// Nous définissons ici les paramètres du serveur.
var hostname = 'localhost';
var port = 3000;

// Nous créons un objet de type Express. 
var app = express();

//Afin de faciliter le routage (les URL que nous souhaitons prendre en charge dans notre API), nous créons un objet Router.
//C'est à partir de cet objet myRouter, que nous allons implémenter les méthodes. 
var myRouter = express.Router();

// Je vous rappelle notre route (/piscines).  
myRouter.route('/getlistbyid')
	// J'implémente les méthodes GET, PUT, UPDATE et DELETE
	// GET
	.get(function (req, res) {
		var id = req.query.listid;
		const listManager = new ListManager();
		var promise = listManager.getListById(id);
		promise.then(list => {
			res.json(list);
		},
			error => {
				res.status(500).send('' + error);
			});
	})

myRouter.route('/getlists')
	// J'implémente les méthodes GET, PUT, UPDATE et DELETE
	// GET
	.get(function (req, res) {
		const listManager = new ListManager();
		var promise = listManager.geAllLists();
		promise.then(list => {
			res.json(list);
		},
			error => {
				console.log(error)
				res.status(500).send('' + error);
			});
	})

// Nous demandons à l'application d'utiliser notre routeur
app.use(myRouter);

// Démarrer le serveur 
app.listen(port, hostname, function () {
	console.log("Mon serveur fonctionne sur http://" + hostname + ":" + port);
});