import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Ionic from "@ionic/vue";
import "@ionic/core/css/ionic.bundle.css";
import AllMdIcon from 'vue-ionicons/dist/ionicons-md.js'
import store from './store/store.js';

Vue.use(Ionic);
Vue.use(AllMdIcon)
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
