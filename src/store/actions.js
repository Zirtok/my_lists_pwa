const resetCurrentList = (context) => {
  context.commit("changeCurrentList", undefined);
};
const deleteList = (context,list) => {
  var allLists = context.getters.allLists;
  for (var i=0; i < allLists.length; i++) {
    if (allLists[i] === list) {
      allLists.splice(i, 1); 
    }
  }
  context.commit("updateAllLists", allLists);
};

const deleteItemFromCurrentList = (context,itemId) => {

  var allLists = context.getters.allLists;
  var currentList = context.getters.currentList;
  var indexCurrentList;
  for (var j=0; j < allLists.length; j++) {
    if (allLists[j] === currentList) {
      allLists.splice(j, 1);
      indexCurrentList = j>0? j-1 : j;
    }
  }
  for (var i=0; i < currentList.items.length; i++) {
    if (currentList.items[i].id === itemId) {
      currentList.items.splice(i, 1);
      allLists.splice(indexCurrentList,0,currentList); 
    }
  }


  context.commit("updateAllLists", allLists);
};

const addList = (context, nom) => {
  var newList = {
    "name": nom,
    "items": []
  }
  var allLists = context.getters.allLists;
  allLists.splice(allLists.length,0,newList);
  context.commit("updateAllLists", allLists);
}

const addItem = (context, nom) => {

  // to replace after creating backend
  function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
  
  var newItem = {
    "name": nom,

    // to replace after creating backend
    "id": uuidv4(),
  }
  var allLists = context.getters.allLists;
  var currentList = context.getters.currentList;
  var indexCurrentList;
  for (var j=0; j < allLists.length; j++) {
    if (allLists[j] === currentList) {
      allLists.splice(j, 1);
      indexCurrentList = j>0? j-1 : j;
    }
  }
  currentList.items.splice(currentList.length,0,newItem);
  allLists.splice(indexCurrentList,0,currentList); 
  context.commit("updateAllLists", allLists);
}


const updateItem = (context, item) => {
  var allLists = context.getters.allLists;
  var currentList = context.getters.currentList;
  for (var i=0; i < currentList.items.length; i++) {
    if (currentList.items[i].id === item.id) {
      currentList.items[i] = item;
    }
  }
  for (var j=0; j < allLists.length; j++) {
    if (allLists[j] === currentList) {
      allLists[j] = currentList;
    }
  }
  context.commit("changeCurrentList", currentList);
  context.commit("updateAllLists", allLists);
}

const updateList = (context, list) => {
  var allLists = context.getters.allLists;
  var currentList = context.getters.currentList;
  for (var j=0; j < allLists.length; j++) {
    if (allLists[j] === currentList) {
      allLists[j] = list;
    }
  }
  context.commit("changeCurrentList", currentList);
  context.commit("updateAllLists", allLists);
}


export default {
  resetCurrentList,
  deleteList,
  deleteItemFromCurrentList,
  addList,
  addItem,
  updateItem,
  updateList,
};
