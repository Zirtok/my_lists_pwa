// eslint-disable-next-line
const currentList = state => state.currentList ;

const editState = state => state.editState ;

const allLists = state => state.allLists ;

const isMainPage = state => state.isMainPage ;



export default {
  currentList,
  editState,
  allLists,
  isMainPage,
};