/* eslint-disable */
const changeCurrentList = (state, list) => {
    state.currentList = list
};

const updateEditState = (state, bool) => {
    state.editState = bool
};
const updateAllLists = (state, allLists) => {
    state.allLists = allLists
};

const updateIsMainPage = (state, bool) => {
    state.isMainPage = bool
};
export default {
    changeCurrentList,
    updateEditState,
    updateAllLists,
    updateIsMainPage,
};
