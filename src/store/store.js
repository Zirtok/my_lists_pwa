import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters.js'
import actions from './actions.js'
import mutations from './mutations.js'

Vue.use(Vuex)
Vue.config.devtools = true

export default new Vuex.Store({
    state : {
        allLists: [],
        currentList : {},
        editState : false,
        isMainPage: true,
    },
    getters,
    actions,
    mutations
})